# Help a Neighbour

This project aims to connect vulnerable people that are struggling to leave their house during the current pandemic with those that are willing to help. Whether that be by dropping off food, having a chat, or in any other way. It's important in times like these for people to come together.

## Setting up for Windows users

1. Go to https://gitforwindows.org/ and install Git Bash.
2. After installation, please clone the repo using the following command in Git Bash: git clone https://gitlab.com/DrRoach/help-a-neighbour.git
3. Download and Install the latest Thread Safe version of PHP from https://windows.php.net/download/
4. Download Composer from https://getcomposer.org/download/
5. Change directory to the location where the help-a-neighbour project is in your system in Git Bash by using this command:`cd {file location here}`
6. Type the following commands, one by one in Git Bash:
           npm install
           npm run dev 
           cp .env.example .env
           composer install
           php artisan key:generate
           php artisan dusk:install
7. Go into the `/public` directory and run `php -S localhost:8080` or `php -S localhost:8888`
8. Visit `http://localhost:8080` or `http://localhost:8888` in your browser to see the website

## Setting up for *nix users

1. Clone the repo
2. Run make install
3. Go into the /public directory and run php -S localhost:8080
4. Visit http://localhost:8080 in your browser


## Contributing

### Finding a task to work on

There is an issue board in the project gitlab. If you are going to work on an issue **please assign it to yourself and move it into the doing board**. This will stop multiple people working on the same job. Once it is done **create a pull request and move the issue into the done board**. Pull request will only be merged if there are tests which pass.

### Writing Code

Both the `master` and `dev` branches are protected, meaning nobody can push to them. This is to enforce a Pull Request workflow. You should work as follows:

1. Create a new branch from `dev` following the structure: `{feature|hotix}/{BRANCH_NAME}`
    - Example: `git checkout dev && git checkout -b feature/readme-improvements`
2. Commit as often as possible to your branch as you work.
3. Merge the `dev` branch into your branch as frequently as possible:
    - Example: `git checkout feature/readme-improvements && git merge dev`
4. When you think you are done, run `php artisan dusk tests/`. If any of the tests fail, fix them.
5. When done push your branch and create a pull request in Gitlab.
6. Fix issues if any are flagged in pull request
7. Your work is merged!

### Why like this?

This is so that we can choose to group updates into a single release. This process may change in the future and if it does the README will be updated. It will also help to reduce the number of merge conflicts that may happen.

## Software used

The backend is Laravel (PHP). This is so we can put most of the long running/computationally expensive tasks into queues to keep server costs down.

The frontend is still not decided upon and currently uses plain HTML and JS/jQuery.

Laravel dusk is used for testing. Dusk only effects browser and feature tests and documentation can be found [here](https://laravel.com/docs/5.8/dusk). To run tests use the command: `php artisan dusk tests`
