install:
	npm install
	npm run dev
	cp .env.example .env
	composer install
	php artisan key:generate
	php artisan dusk:install
	php artisan migrate
