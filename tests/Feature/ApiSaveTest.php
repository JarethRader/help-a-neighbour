<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;

class ApiSaveTest extends TestCase
{
    use RefreshDatabase;

    public function testSaveHelper()
    {
        $response = $this->post('/api/save', [
            'help_type' => "helper",
            'name' => "Testing Name",
            'address' => "123 Test Address",
            'email' => 'testing@testing.com',
            'phone' => '0123456789',
            'offering' => [
                'chat'
            ]
        ]);

        $response->assertStatus(200);

        $response->assertJson([
            'created' => true
        ]);
    }

    public function testSaveEmailExists()
    {
        $user = factory(User::class)->create([
            'email' => 'testingemail@gmail.com',
        ]);

        $response = $this->post('/api/save', [
            'help_type' => 'helper',
            'name' => "Diff Name",
            'address' => "123 New Address",
            'email' => 'testingemail@gmail.com',
            'phone' => '0987654321',
            'offering' => json_encode(['chat', 'shopping'])
        ]);

        $response->assertStatus(422);

        $response->assertJson([
            'errors' => [
                'email' => [
                    "That email address has already been submitted."
                ]
            ]
        ]);

        $user->delete();
    }

    public function testSavePhoneNumberExists()
    {
        $user = factory(User::class)->create([
            'phone' => '0123456789'
        ]);

        $response = $this->post('/api/save', [
            'help_type' => 'needs_help',
            'name' => "Diff Name",
            'address' => '1234 Adddress Road',
            'email' => 'randomemailaddress@gmail.com',
            'phone' => '0123456789',
            'offering' => [
                'chat'
            ]
        ]);

        $response->assertStatus(422);

        $response->assertJson([
            'errors' => [
                'phone' => [
                    "That phone number has already been submitted."
                ]
            ]
        ]);
    }
}
