<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use \App\Http\Controllers\WelcomeController;

class WelcomeControllerTest extends TestCase
{
    public function testGetUserIpAddr()
    {
        $welcomeController = new WelcomeController();
        $this->assertTrue(!empty($welcomeController->getUserIpAddr()));
    }
}
