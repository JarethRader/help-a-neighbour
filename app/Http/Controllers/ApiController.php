<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\ProcessUser;
use App\Http\Requests\UserSaveRequest;
use GuzzleHttp\Client;

class ApiController extends Controller
{
    public function save(UserSaveRequest $request)
    {

        // If the token is passed then we know we're on production. Form handling done in 
        //  UserSaveRequest
        if (!empty($request->token)) {
            $client = new Client([
                'base_uri' => 'https://www.google.com',
                'timeout'  => 2.0,
            ]);

            $response = $client->request('POST', '/recaptcha/api/siteverify', [
                'form_params' => [
                    'secret' => env('RECAPTCHA_SECRET_KEY'),
                    'response' => $request->token
                ]
            ]);

            $body = $response->getBody();
            $decodedBody = json_decode($body);
            if (!$decodedBody->success || $decodedBody->score < .5 || $decodedBody->action !== "homepage") {

                return response()->json([
                    'created' => false,
                    'message' => "Sorry you failed the reCAPTCHA. Please try again."
                ]);
            }
        }

        ProcessUser::dispatch($request);

        return response()->json([
            'created' => true
        ]);
    }
}
