<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class WelcomeController extends Controller
{
    public function get(Request $request)
    {
        if ($this->getUserIpAddr() == "127.0.0.1" || $this->getUserIpAddr() == "::1") {
            $usersInNeed = rand(0, 9999);
        } else {
            $usersInNeed = User::where('help_type', 'needs_help')
                ->count();
        }

        return view('welcome', [
            'usersInNeed' => $usersInNeed
        ]);
    }

    public function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (!empty($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $ip = "unknown";
        }

        return $ip;
    }

}
