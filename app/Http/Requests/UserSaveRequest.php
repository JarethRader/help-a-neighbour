<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequest;
use Illuminate\Validation\Rule;

class UserSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $resp = [
            'help_type' => [
                'required',
                Rule::in(['helper', 'needs_help'])
            ],
            'name' => "required|string",
            'address' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|numeric|unique:users,phone',
            'offering' => 'required|array'
        ];

        if (env('APP_ENV') == 'prod') {
            $resp['token'] = 'required|string';
        }

        return $resp;
    }

    public function messages()
    {
        return [
            'token.required' => 'Sorry you failed the reCAPTCHA. Please try again.',
            'email.unique' => 'That email address has already been submitted.',
            'phone.unique' => 'That phone number has already been submitted.',
        ];
    }
}
