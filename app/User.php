<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'help_type',
        'name',
        'address',
        'email',
        'phone',
        'offering'
    ];

    public function getOfferingAttribute($value)
    {
        return json_decode($value);
    }

    public function setOfferingAttribute($value)
    {
        $this->attributes['offering'] = json_encode($value);
    }
}
