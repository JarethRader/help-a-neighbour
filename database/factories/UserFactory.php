<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    $offeringArray = ['chat', 'shopping'];

    return [
        'help_type' => $faker->randomElement(['helper', 'needs_help']),
        'name' => $faker->name,
        'address' => $faker->address,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'offering' => $faker->randomElements($offeringArray, rand(1, count($offeringArray)))
    ];
});

$factory->state(User::class, 'helper', [
    'help_type' => 'helper'
]);

$factory->state(User::class, 'needs_help', [
    'help_type' => 'needs_help'
]);
