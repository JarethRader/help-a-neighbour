<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="m-0 p-0">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{ mix('css/app.css')}}">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Helping Hand') }}</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class='flex-center position-ref full-height'>
            <div class='content'>
                <div id="index" class="title m-b-md">
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript" src={{mix("js/app.js")}}></script>
</html>
