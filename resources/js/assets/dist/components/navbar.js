"use strict";
/**
 * @Author Jareth Rader
 * @desc This is the navbar component. It gets the navigation object from its parent.
 * It renders out the page names and and routes to them accordingly
 *
 *  @NOTE If you want to change the Link name or the Route, please change the navigation object in resources/js/assets/src/utils/navigation
 */
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const Navbar = (props) => {
    const { brand, links } = props;
    const NavLinks = () => links.map((link) => (React.createElement("li", { key: link.name, style: { listStyleType: "none" } },
        React.createElement("a", { href: link.to }, link.name))));
    return (React.createElement("nav", { className: "navbar navbar-light bg-light" },
        React.createElement("a", { href: brand.to }, brand.name),
        React.createElement(NavLinks, null)));
};
exports.default = Navbar;
