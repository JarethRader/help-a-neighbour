"use strict";
/**
 * @Author Jareth Rader
 * @desc This file define the navigation structure, creating an object that define
 * the page names and routes for the different pages for the navbar
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @desc this is the navigation object. You can change the names to change what the navbar links say, and the "to" part to change the route
 */
const navigation = {
    brand: { name: "Helping Hand", to: "/" },
    links: [
        { name: "Jobs", to: "/jobs" },
        { name: "About Us", to: "/about" },
        { name: "Login", to: "/login" }
    ]
};
exports.default = navigation;
