"use strict";
/**
 * @Author Jareth Rader (JarethRader@gmail.com)
 * @desc This is the very top level component. Every other component will be brought into here.
 * It mainly has the navbar, react-router, and other higher level pages for the site.
 */
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const react_router_dom_1 = require("react-router-dom");
const navbar_1 = __importDefault(require("./components/navbar"));
const navigation_1 = __importDefault(require("./utils/navigation"));
const home_1 = __importDefault(require("./routes/home"));
class Index extends React.Component {
    render() {
        return (React.createElement("div", null,
            React.createElement("h1", { className: "text-justify" }, "Hello World"),
            React.createElement(react_router_dom_1.BrowserRouter, null,
                React.createElement("div", null,
                    React.createElement(navbar_1.default, { brand: navigation_1.default.brand, links: navigation_1.default.links }),
                    React.createElement("div", null,
                        React.createElement(react_router_dom_1.Switch, null,
                            React.createElement(react_router_dom_1.Route, { path: "/" },
                                React.createElement(home_1.default, null)))),
                    React.createElement("footer", null)))));
    }
}
exports.default = Index;
