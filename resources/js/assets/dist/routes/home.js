"use strict";
/**
 * @Author Jareth Rader
 * @desc This is the landing page component
 */
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
class Home extends React.Component {
    componentDidMount() {
        this.setState({ _mounted: true });
    }
    componentWillUnmount() {
        this.setState({ _mounted: false });
    }
    render() {
        return (React.createElement("div", { className: "container" },
            React.createElement("h1", null, "Home Page")));
    }
}
exports.default = Home;
