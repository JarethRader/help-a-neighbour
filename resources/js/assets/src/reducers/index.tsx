//Root Reducer for Redux, combines all other reducers into our root reducer
import { combineReducers } from "redux";
import userReducer from "./userReducer";

//Add all reducers to this function below
export default combineReducers({
    user: userReducer
});
