/**
 * @Author Jareth Rader
 * @desc This is the landing page component
 */

import * as React from "react";

/**
 * @desc It's a good idea to keep track of is a component is mounted or not, then you can prevent it from trying to make changes to state
 * if it isn't mounted
 */
type homeState = {
    _mounted: boolean;
};

class Home extends React.Component<{}, homeState> {
    componentDidMount() {
        this.setState({ _mounted: true });
    }

    componentWillUnmount() {
        this.setState({ _mounted: false });
    }

    render() {
        return (
            <div
                className="text-center pb-5"
                style={{
                    backgroundImage:
                        "linear-gradient(to bottom, white, #86FFFF)"
                }}
            >
                <img
                    style={{ width: "25%" }}
                    src={require("../inc/HelpingHand.webp")}
                />
                <h3>Where neighbors become friends</h3>
                <hr className="my-1" />
                <img
                    style={{ width: "30%" }}
                    src={require("../inc/HelpingHandIcon.png")}
                />
                <h5>
                    In these time of epidemia and lockdown you may be in need of
                    someone to help you do something or just buy some groceries.
                    Tell us your needs and personnal information and we will do
                    our best to match you with someone close to where you're
                    living who can help you with that!
                </h5>
                <p className="mb-0">
                    <a className="btn btn-dark" href="/login" role="button">
                        Reach Out
                    </a>
                </p>
            </div>
        );
    }
}

export default Home;
