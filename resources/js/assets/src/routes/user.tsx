/**
 * @Author Jareth Rader
 * @desc This is the landing page component
 */

import * as React from "react";
/**
 * @desc It's a good idea to keep track of is a component is mounted or not, then you can prevent it from trying to make changes to state
 * if it isn't mounted
 */
type userState = {
    _mounted: boolean;
};

class User extends React.Component<{}, userState> {
    componentDidMount() {
        this.setState({ _mounted: true });
    }

    componentWillUnmount() {
        this.setState({ _mounted: false });
    }

    render() {
        return <div>UserPage</div>;
    }
}

export default User;
