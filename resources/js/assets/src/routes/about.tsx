/**
 * @Author Jareth Rader
 * @desc This is the about page component
 */

import * as React from "react";

/**
 * @desc It's a good idea to keep track of is a component is mounted or not, then you can prevent it from trying to make changes to state
 * if it isn't mounted
 */
type aboutState = {
    _mounted: boolean;
};

class About extends React.Component<{}, aboutState> {
    componentDidMount() {
        this.setState({ _mounted: true });
    }

    componentWillUnmount() {
        this.setState({ _mounted: false });
    }

    render() {
        return (
            <div className="container center">
                <h1>About Page</h1>
            </div>
        );
    }
}

export default About;
