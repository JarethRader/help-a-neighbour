/**
 * @Author Jareth Rader
 * @desc This is the 404 page that is displayed is a user tries to navigate to a nonexistant route on the site
 */

import * as React from "react";

class NotFound extends React.Component<{}, {}> {
    render() {
        return (
            <div
                className="d-flex flex-row align-items-center"
                style={{ minHeight: "100vh", background: "#dedede" }}
            >
                <div className="row justify-content-center">
                    <div className="col-md-12 text-center">
                        <span className="display-1 d-block">404</span>
                        <div className="mb-4 lead">
                            The page you are looking for was not found.
                        </div>
                        <button className="px-8 py-4 bg-black text-white">
                            Back to Home
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default NotFound;
