/**
 * @Author Jareth Rader (JarethRader@gmail.com)
 * @desc This is the very top level component. Every other component will be brought into here.
 * It mainly has the navbar, react-router, and other higher level pages for the site.
 */

import * as React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

/**
 * @desc Sidebar and footer files
 */
import Navbar from "./components/navbar";
import Footer from "./components/footer";
import navigation from "./utils/navigation";

/**
 * @desc Roues for pages
 */
import Home from "./routes/home";
import About from "./routes/about";
import User from "./routes/user";
import NotFound from "./routes/notFound";

/**
 * @desc Redux dependencies
 */
import { Provider } from "react-redux";
import store from "./store";

class Index extends React.Component<{}, {}> {
    render() {
        return (
            <Provider store={store}>
                <div id="App">
                    {/**
                     * @desc This is the react router componenet. When you add a new page to the site you will need to add it to the Switch element
                     * You will also need to add the name and route to the navigation object so it will be added to the navbar
                     */}
                    <BrowserRouter>
                        <div id="Page">
                            <Navbar
                                brand={navigation.brand}
                                links={navigation.links}
                            />

                            {/* can add an error modal in here also */}
                            <Switch>
                                <Route exact path="/user">
                                    <User />
                                </Route>
                                <Route exact path="/about">
                                    <About />
                                </Route>
                                <Route exact path="/">
                                    <Home />
                                </Route>
                                <Route path="*">
                                    <NotFound />
                                </Route>
                            </Switch>
                            <Footer
                                brand={navigation.brand}
                                links={navigation.links}
                            />
                        </div>
                    </BrowserRouter>
                </div>
            </Provider>
        );
    }
}

export default Index;
