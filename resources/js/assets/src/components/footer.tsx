/**
 * @Author Jareth Rader
 * @Desc This is the footer component. It functions similar to the sidebar
 */

import * as React from "react";

import pair from "../utils/pair";

const Footer = (props: { brand: pair; links: Array<pair> }) => {
    const { brand, links } = props;

    const FooterLinks: any = () =>
        links.map((link: pair) => (
            <li key={link.name}>
                <a
                    style={{
                        listStyleType: "none",
                        color: "white"
                    }}
                    href={link.to}
                >
                    {link.name}
                </a>
            </li>
        ));

    return (
        <footer className="page-footer font-small text-white pt-4 bg-dark">
            <div className="container-fluid text-center text-md-left">
                <div className="row mx-5">
                    <div className="col-md-6 mt-md-0 mt-3">
                        <img
                            src={require("../inc/HelpingHand.webp")}
                            alt={brand.name}
                            style={{ width: "10rem" }}
                        />
                        <p>Footer McFootFace</p>
                    </div>
                    <hr className="clearfix w-100 d-md-none pb-3"></hr>
                    <div className="col-md-3 mp-md-0 mb-3">
                        <ul className="list-unstyled">
                            <FooterLinks />
                        </ul>
                    </div>
                </div>
            </div>
            <div
                className="footer-copyright text-center py-3"
                style={{ backgroundColor: "black" }}
            >
                &copy;
                {new Date().getFullYear()}, HelpingHand.com
            </div>
        </footer>
    );
};

export default Footer;
