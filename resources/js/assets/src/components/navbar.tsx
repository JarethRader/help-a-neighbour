/**
 * @Author Jareth Rader
 * @desc This is the navbar component. It gets the navigation object from its parent.
 * It renders out the page names and and routes to them accordingly
 *
 *  @ATTENTION If you want to change the Link name or the Route, please change the navigation object in resources/js/assets/src/utils/navigation
 */

import * as React from "react";
import { slide as Menu } from "react-burger-menu";
import pair from "../utils/pair";

const Navbar = (props: { brand: pair; links: Array<pair> }) => {
    const { brand, links } = props;

    const NavLinks: any = () =>
        links.map((link: pair) => (
            <div className="sidebar-item" key={link.name}>
                <a className="bm-item" href={link.to}>
                    <div className="sidebar-item-content">{link.name}</div>
                </a>
            </div>
        ));

    return (
        <Menu
            outerContainerId={"App"}
            pageWrapId={"Page"}
            customCrossIcon={false}
            menuClassName={"bm-menu sidebar-list"}
            itemListClassName={"bm-item-list"}
        >
            <a className="sidebar-brand-item" href={brand.to}>
                <img
                    style={{ width: "12.5rem" }}
                    src={require("../inc/HelpingHand.webp")}
                    alt={brand.name}
                />
            </a>
            <NavLinks />
        </Menu>
    );
};

export default Navbar;
