/**
 * @Author Jareth Rader
 * @desc This file define the navigation structure, creating an object that define
 * the page names and routes for the different pages for the navbar
 */

/**
 * @desc the thought with this regex bit was to validate the paths, but there isn't an easy way to do that with typescript so I didn't use it
 */
// let routeRegex: RegExp = /^\/([A-Za-z])*/;

import pair from "./pair";

/**
 * @desc this interface defines the type for naviagtion object
 */
interface navigationObj {
    brand: pair;
    links: pair[];
}

/**
 * @desc this is the navigation object. You can change the names to change what the navbar links say, and the "to" part to change the route
 */
const navigation: navigationObj = {
    brand: { name: "Helping Hand", to: "/" },
    links: [
        { name: "About Us", to: "/about" },
        { name: "Near Me", to: "/map" },
        { name: "Jobs", to: "/jobs" },
        { name: "Account", to: "/user" }
    ]
};

export default navigation;
