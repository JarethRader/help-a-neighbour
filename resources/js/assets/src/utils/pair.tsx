/**
 * @desc This interface defines the type for the pair objects for the navigation object and to check the type thourghout the various components
 */
export default interface pair {
    name: string;
    to: string;
}
