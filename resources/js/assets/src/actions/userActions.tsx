/**
 * @Author Jareth Rader
 * @desc These are the actions for user athentication
 */

import axios from "axios";
import {
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    USER_LOADING,
    SignInTypes,
    loginObj,
    registerObj
} from "./types";

// export const register: SignInTypes = (userObj: registerObj) => {
//     try {
//         if
//     }
// }

export const setUserLoading = () => {
    return {
        type: USER_LOADING
    };
};
