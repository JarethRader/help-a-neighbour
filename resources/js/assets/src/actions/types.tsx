/**
 * @Author Jareth Rader
 * @desc Types/constants need types for actions so we can evaluate them.
 * Everything you do in your application should have an action type
 */

export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAIL = "REGISTER_FAIL";
export const USER_LOADING = "USER_LOADING";

export interface loginObj {
    username: string;
    password: string;
}

export interface registerObj {
    username: string;
    password: string;
    email: string;
    phoneNumber: string;
}

interface loginAction {
    type: typeof LOGIN_SUCCESS | typeof LOGIN_FAIL;
    payload: loginObj;
}

interface registerAction {
    type: typeof REGISTER_SUCCESS | typeof REGISTER_FAIL;
    paylaod: registerObj;
}

export type SignInTypes = loginAction | registerAction;
